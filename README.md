# Masana, recruitment test


👋 Hello, if you get here you probably had a first round of technical questions 
to join [masana](https://www.masana.care/). If that's not the case, you probably 
won't find anything helpful here. 

This project contains two main branches. They both contain the same exercice but 
one is dedicated for Scala developers while the other is for Java developers. 
Pick the one that you are more comfortable with:  

 + **[main-scala](https://gitlab.com/masana2/masana-recruitment-test/-/tree/main-scala)**, for _Scala_ developers
 + **[main-java](https://gitlab.com/masana2/masana-recruitment-test/-/tree/main-java)**, for _Java_ developers
